package eu.europa.ec.sante.openncp.portal.controller;

import eu.europa.ec.sante.openncp.application.client.connector.ClientConnectorService;
import eu.europa.ec.sante.openncp.application.client.connector.request.*;
import eu.europa.ec.sante.openncp.common.security.AssertionType;
import eu.europa.ec.sante.openncp.core.client.api.ObjectFactory;
import eu.europa.ec.sante.openncp.core.client.api.PatientId;
import eu.europa.ec.sante.openncp.portal.configuration.ApplicationProperties;
import eu.europa.ec.sante.openncp.portal.mock.MockService;
import org.apache.commons.lang3.StringUtils;
import org.opensaml.saml.saml2.core.Assertion;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;


import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/fhir")
public class PortalFHIRController {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    private final ClientConnectorService clientConnectorService;

    private final RestTemplate restTemplate;

    private final MockService mockService;

    private final ApplicationProperties applicationProperties;

    public static final String TOMCAT_PATH = "/opt/openncp-configuration/dicom-files/";


    public PortalFHIRController(RestTemplate restTemplate, MockService mockService, ApplicationProperties applicationProperties, ClientConnectorService clientConnectorService) {
        this.restTemplate = restTemplate;
        this.mockService = mockService;
        this.applicationProperties = applicationProperties;
        this.clientConnectorService = clientConnectorService;
    }

    @PostConstruct
    public void init() {
    }

    @GetMapping("/Patient")
    public ResponseEntity<?> proxyPatientRequest(HttpServletRequest request,
                                                      @RequestParam Map<String, String> params) {
        final Map<AssertionType, Assertion> assertionMap = new HashMap<>();
        assertionMap.put(AssertionType.HCP, mockService.generateHcpAssertion());
        return clientConnectorService.queryPatientFhir(assertionMap, request.getHeader("CountryCode"), params);
    }

    @GetMapping("/Bundle")
    public ResponseEntity<?> proxyBundleRequest(HttpServletRequest request,  @RequestParam Map<String, String> params) {
        final Map<AssertionType, Assertion> assertionMap = new HashMap<>();
        final Assertion hcpAssertion = mockService.generateHcpAssertion();
        assertionMap.put(AssertionType.HCP, hcpAssertion);
        final PatientId patientId = buildPatientId(params.get("patient.identifier"));
        assertionMap.put(AssertionType.TRC, mockService.generateTreatmentConfirmationAssertion(hcpAssertion, patientId));
        return clientConnectorService.queryBundleFhir(assertionMap, request.getHeader("CountryCode"), params);
    }

    @GetMapping("/Bundle/{id}")
    public ResponseEntity<?> proxyBundleRequest(HttpServletRequest request, @PathVariable("id") final String id) {
        final Map<AssertionType, Assertion> assertionMap = new HashMap<>();
        final Assertion hcpAssertion = mockService.generateHcpAssertion();
        assertionMap.put(AssertionType.HCP, hcpAssertion);
        final PatientId patientId = buildPatientId(request.getHeader("PatientIdentifier"));
        assertionMap.put(AssertionType.TRC, mockService.generateTreatmentConfirmationAssertion(hcpAssertion, patientId));
        return clientConnectorService.queryBundleFhirById(assertionMap, request.getHeader("CountryCode"), id);
    }

    @GetMapping("/DocumentReference")
    public ResponseEntity<?> proxyDocumentReferenceRequest(HttpServletRequest request,
                                                           @RequestParam Map<String, String> params) {
        final Map<AssertionType, Assertion> assertionMap = new HashMap<>();
        final Assertion hcpAssertion = mockService.generateHcpAssertion();
        assertionMap.put(AssertionType.HCP, hcpAssertion);
        final PatientId patientId = buildPatientId(params.get("patient.identifier"));
        assertionMap.put(AssertionType.TRC, mockService.generateTreatmentConfirmationAssertion(hcpAssertion, patientId));
        return clientConnectorService.queryDocumentReferenceFhir(assertionMap, request.getHeader("CountryCode"), params);
    }

    @GetMapping("/DocumentReference/{id}")
    public ResponseEntity<?> proxyDocumentReferenceByIdRequest(HttpServletRequest request, @PathVariable("id") String id,
                                                               @RequestParam Map<String, String> params) {
        final Assertion hcpAssertion = mockService.generateHcpAssertion();
        final PatientId patientId = buildPatientId(params.get("patient.identifier"));


        final DocumentReferenceByIdRequest documentReferenceByIdRequest = ImmutableDocumentReferenceByIdRequest.builder()
                .countryCode(request.getHeader("CountryCode"))
                .patientId(patientId)
                .id(id)
                .putAssertion(AssertionType.HCP, hcpAssertion)
                .build();
        return clientConnectorService.queryDocumentReferenceByIdFhir(documentReferenceByIdRequest);
    }

    @GetMapping("/MedicalImagingStudy")
    public ResponseEntity<?> proxyMedicalImagingStudy(HttpServletRequest request,
                                                           @RequestParam Map<String, String> params) {
        final Assertion hcpAssertion = mockService.generateHcpAssertion();
        final PatientId patientId = buildPatientId(params.get("patient.identifier"));


        final MedicalImagingStudyRequest medicalImagingStudyRequest = ImmutableMedicalImagingStudyRequest.builder()
                .countryCode(request.getHeader("CountryCode"))
                .patientId(patientId)
                .procedureDateBetweenRange(ImmutableDateRange.builder()
                        .from(LocalDate.parse(params.get("from"), dateTimeFormatter))
                        .to(LocalDate.parse(params.get("to"), dateTimeFormatter))
                        .build()
                )
                .putAssertion(AssertionType.HCP, hcpAssertion)
                .modalityCode(params.get("modalityCode"))
                .bodyPartCode(params.get("bodyPartCode"))
                .build();

        return clientConnectorService.queryMedicalImagingStudyDocumentReferences(medicalImagingStudyRequest);
    }


    @CrossOrigin(origins = "*")
    @GetMapping("/DisplayMedicalImage")
    public ResponseEntity<byte[]> proxyDisplayMedicalImage(HttpServletRequest request,
                                                           @RequestParam Map<String, String> params) {
        try {
            final Assertion hcpAssertion = mockService.generateHcpAssertion();
            final PatientId patientId = buildPatientId(params.get("patient.identifier"));

            String studyUid = params.get("studyUid");
            String seriesUid = params.get("seriesUid");
            String instanceUid = params.get("instanceUid");

            final FetchMedicalImagesRequest fetchMedicalImagesRequest = ImmutableFetchMedicalImagesRequest.builder()
                    .countryCode("BE")
                    .patientId(patientId)
                    .studyUid(studyUid)
                    .seriesUid(seriesUid)
                    .instanceUid(instanceUid)
                    .putAssertion(AssertionType.HCP, hcpAssertion)
                    .build();

            byte[] dicomBytes = clientConnectorService.fetchMedicalImagesRequest(fetchMedicalImagesRequest).getBody();

            String fileName = "DICOM_" + System.currentTimeMillis() + ".dcm";
            String filePath = TOMCAT_PATH + fileName;

            Files.createDirectories(Paths.get(TOMCAT_PATH));

            try (FileOutputStream fos = new FileOutputStream(new File(filePath))) {
                fos.write(dicomBytes);
            }

            return ResponseEntity.ok().body(dicomBytes);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
        }
    }


    private PatientId buildPatientId(String patientIdentifier) {
        String decodedString = URLDecoder.decode(patientIdentifier, StandardCharsets.UTF_8);
        final String[] ids = StringUtils.split(decodedString, "|");
        final ObjectFactory objectFactory = new ObjectFactory();
        final PatientId patientId = objectFactory.createPatientId();
        patientId.setRoot(ids[0]);
        patientId.setExtension(ids[1]);
        return patientId;
    }
}
