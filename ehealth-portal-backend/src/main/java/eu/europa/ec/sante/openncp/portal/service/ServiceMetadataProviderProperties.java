package eu.europa.ec.sante.openncp.portal.service;

public class ServiceMetadataProviderProperties {

    private ServiceMetadataProviderProperties() {
    }

    public static final String GTW_TLS_CLIENT_CERT_ALIAS = "GTW_TLS_CLIENT_CERT_ALIAS";
    public static final String GTW_TLS_CLIENT_CERT_PWD = "GTW_TLS_CLIENT_CERT_PWD";
    public static final String GTW_TLS_CLIENT_KEYSTORE_PATH = "GTW_TLS_CLIENT_KEYSTORE_PATH";
    public static final String GTW_TLS_CLIENT_KEYSTORE_PWD = "GTW_TLS_CLIENT_KEYSTORE_PWD";
    public static final String GTW_TRUSTSTORE_PATH = "GTW_TRUSTSTORE_PATH";
    public static final String GTW_TRUSTSTORE_PWD = "GTW_TRUSTSTORE_PWD";
}
