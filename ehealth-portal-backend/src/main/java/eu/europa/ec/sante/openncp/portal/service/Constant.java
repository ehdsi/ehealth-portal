package eu.europa.ec.sante.openncp.portal.service;

public class Constant {

    public static final String PS_CLASS_CODE = "";
    public static final String EP_CLASS_CODE = "";

    private Constant() {
        // Private Constructor preventing instantiating.
    }
}
