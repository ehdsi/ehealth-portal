package eu.europa.ec.sante.openncp.portal.model;

public class PackageSize {

    private String quantity;
    private String packageSizeL1;
    private String packageSizeL2;
    private String packageSizeL3;

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPackageSizeL1() {
        return packageSizeL1;
    }

    public void setPackageSizeL1(String packageSizeL1) {
        this.packageSizeL1 = packageSizeL1;
    }

    public String getPackageSizeL2() {
        return packageSizeL2;
    }

    public void setPackageSizeL2(String packageSizeL2) {
        this.packageSizeL2 = packageSizeL2;
    }

    public String getPackageSizeL3() {
        return packageSizeL3;
    }

    public void setPackageSizeL3(String packageSizeL3) {
        this.packageSizeL3 = packageSizeL3;
    }
}
